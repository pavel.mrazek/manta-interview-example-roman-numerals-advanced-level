import java.util.Optional;

/**
 * Enum representing the supported Roman numeric symbols and their corresponding decimal values.
 * The order, in which the constants are defined, is important.
 */
public enum RomanNumeralOrder {
    I(1),
    V(5),
    X(10),
    L(50),
    C(100),
    D(500),
    M(1000);

    /** Decimal value corresponding to the roman number. */
    private final int decimalValue;

    RomanNumeralOrder(final int decimalValue){
        this.decimalValue = decimalValue;
    }

    /** Returns the enum constant holding the highest value. */
    public static RomanNumeralOrder getHighestOrder(){
        return RomanNumeralOrder.values()[RomanNumeralOrder.values().length-1];
    }

    /**
     * Returns lower Roman order if it exists. It means an order precedent to this. Lower order is undefined for Roman symbol "I".
     * Examples: X->V,  D->C,   I->undefined
     */
    public Optional<RomanNumeralOrder> getLowerOrder(){
        final int selfIndex = getIndex();
        // cannot get better than the first element
        if(selfIndex==0){
            return Optional.empty();
        }
        return Optional.of( RomanNumeralOrder.values()[selfIndex-1]);
    }


    /**
     * Returns higher Roman order if it exists. It means an order following this. Higher order is undefined for the currently highest Roman symbol.
     * Examples: I->V,  C->X,  X->L,    D->undefined
     */
    public Optional<RomanNumeralOrder> getHigherOrder(){
        final int selfIndex = getIndex();
        // cannot get later than the last element
        if(selfIndex+1==RomanNumeralOrder.values().length){
            return Optional.empty();
        }
        return Optional.of( RomanNumeralOrder.values()[selfIndex+1]);
    }


    /**
     * Returns the index of this enum constant as looked up among enum's all values.
     * @throws IllegalStateException if not found
     */
    private int getIndex(){
        for(int i = 0; i<RomanNumeralOrder.values().length; i++){
            final RomanNumeralOrder current = RomanNumeralOrder.values()[i];
            // found index if self
            if(this.equals(current)) {
                return i;
            }
        }
        throw new IllegalStateException("Could not find index of RomanNumeralOrder with decimal value " + getDecimalValue());
    }

    /**
     * Returns decimal representation of this roman numeric symbol.
     * Examples:   X -> 10, L -> 50
     */
    public int getDecimalValue() {
        return decimalValue;
    }
}
